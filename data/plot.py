import matplotlib.pyplot as plt
import math

colors = ["#1F78B4", "#FF7F0E", "#2CA02C", "#D63728", "#9467BD", "#A8786E", "#DD97CA"]
dic = {}
dic_mt = {}
dic_ac = {}
dic_pa_cc = {}
dic_pa_sv = {}

def add(dic, prob, prog, graph, iters, time):
    #print graph, prog, iters, time
    if (not prob in dic):
        dic[prob] = {}
    if (not graph in dic[prob]):
        dic[prob][graph] = {}
    dic[prob][graph][prog] = (iters, time)

def add_mt(dic_mt, prob, graph, nthreads, time):
    if (not prob in dic_mt):
        dic_mt[prob] = {}
    if (not graph in dic_mt[prob]):
        dic_mt[prob][graph] = {}
    #print graph, nthreads, time, sr, sr / time
    dic_mt[prob][graph][nthreads] = time

def add_parts(dic_pa, prob, graph, nthreads, parts):
    if (not prob in dic_pa):
        dic_pa[prob] = {}
    if (not graph in dic_pa[prob]):
        dic_pa[prob][graph] = {}
    dic_pa[prob][graph][nthreads] = parts

def parse(filename, prob, prog):
    f = file(filename, "r")
    while (True):
        line = f.readline()
        if (not line): break;

        if (line.startswith("#")):
            cont = line.strip().split(" ")
            graph = cont[1]
            prog = cont[2]
            iters = 0
            time = 0.0
            while (True):
                line = f.readline()
                if (line.startswith("iteration")):
                    iters += 1
                if (line.startswith("elapsed time")):
                    cont = line.strip().split(" ")
                    time = float(cont[2])
                    break
            add(dic, prob, prog, graph, iters, time)
    f.close()

def parse2(filename, prob):
    f = file(filename, "r")
    while (True):
        line = f.readline()
        if (not line): break;

        if (line.startswith("#")):
            cont = line.strip().split(" ")
            graph = cont[1]
            prog = cont[2]
            iters = 0
            time = 0.0
            t_select = 0.0
            while (True):
                line = f.readline()
                if (line.startswith("Iteration")):
                    iters += 1
                    cont = line.strip().split(" ")
                    t_select += float(cont[7]);
                if (line.startswith("Total Running Time:")):
                    cont = line.strip().split(" ")
                    time = float(cont[3])
                    break
            add(dic, prob, prog, graph, iters, time)
            print graph, t_select * 100.0 / time
    f.close()

def parse3(filename, prob, prog):
    f = file(filename, "r")
    while (True):
        line = f.readline()
        if (not line): break;

        if (line.startswith("filename")):
            cont = line.strip().split(" ")
            graph = cont[1].split("/")[-1][4:]
            iters = 0
            time = 0.0
            t_mxv = 0.0
            t_assign = 0.0
            t_extract = 0.0
            t_others = 0.0
            while (True):
                line = f.readline()
                if (line.startswith("Iteration")):
                    iters += 1
                if (line.startswith("total")):
                    cont = line.strip().split(" ");
                    t_mxv += float(cont[5][:-1])
                    t_assign += float(cont[7][:-1])
                    t_extract += float(cont[3][:-1])
                    t_others += float(cont[9][:-1])
                if (line.startswith("Total time:")):
                    cont = line.strip().split(" ")
                    time = float(cont[2])
                    break
            add(dic, prob, prog, graph, iters, time)
            add_parts(dic_pa_sv, prob, graph, 8, (t_mxv, t_assign, t_extract, t_others))
    f.close()

def parse4(filename, prob):
    f = file(filename, "r")
    while (True):
        line = f.readline()
        if (not line): break;
        if (line.startswith("#")):
            cont = line.strip().split(" ")
            graph = cont[1]
            prog = cont[2]
            nthreads = int(cont[3])
            iters = 0
            time = 0.0
            t_mxv = 0.0
            t_assign = 0.0
            t_extract = 0.0
            t_others = 0.0
            while (True):
                line = f.readline()
                if (line.startswith("iteration")):
                    iters += 1
                    cont = line.strip().split(" ");
                    if (len(cont) > 8):
                        t_mxv += float(cont[5])
                        t_assign += float(cont[7])
                        t_extract += float(cont[11])
                        t_others += float(cont[9])
                    else:
                        t_mxv += float(cont[5])                        
                if (line.startswith("elapsed time")):
                    cont = line.strip().split(" ")
                    time = float(cont[2])
                if (line.startswith("Sendrecv") or line.startswith("Allgather")):
                    cont = line.strip().split(" ")
                    sr = float(cont[1][:-1])
                    break
            t_assign += sr
            #t_others = time - t_mxv - t_assign - t_extract;
            add_mt(dic_mt, prob, graph, nthreads, time)
            add_parts(dic_pa_cc, prob, graph, nthreads, (t_mxv, t_assign, t_extract, t_others))
    f.close()

# number of active vertices in the graph
def parse5(filename, prob):
    f = file(filename, "r")
    while (True):
        line = f.readline()
        if (not line): break;
        if (line.startswith("#")):
            cont = line.strip().split(" ")
            graph = cont[1]
            prog = cont[2]
            iters = 0
            time = 0.0
            nvals = []
            verts = []
            while (True):
                line = f.readline()
                if (line.startswith("iteration")):
                    cont = line.strip().split()
                    nvals.append(int(cont[3]))
                    if (nvals[-1] > 0):
                        verts.append(int(cont[-1]))
                    iters += 1
                if (line.startswith("elapsed time")):
                    cont = line.strip().split(" ")
                    time = float(cont[2])
                    break
            if (not prob in dic_ac):
                dic_ac[prob] = {}
            dic_ac[prob][graph] = (nvals[:-1], verts)
    f.close()

def avg(lst):
    print sum(lst) / len(lst)

graphs = ["road", "twitter", "web", "kron", "urand"]
nums = [23947347, 61578415, 50636151, 134217726, 134217728]

def plot_iters():
    w = 0.25
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    y_msf = [dic["MSF"][g]["fast-msf"][0] for g in graphs]
    y_cc = [dic["CC"][g]["fast-cc"][0] for g in graphs]
    y_sv = [dic["CC"][g]["comb-sv64"][0] for g in graphs]
    xs = [i-1.5*w for i in range(0, len(graphs))]
    ax.bar(xs, y_msf, width=w, label="Boruvka-MSF", color=colors[1]);
    xs = [i-0.5*w for i in range(0, len(graphs))]
    ax.bar(xs, y_sv, width=w, label="FastSV", color=colors[0]);
    xs = [i+0.5*w for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="Boruvka-CC", color=colors[2]);
    for i in range(0, len(graphs)):
        z = y_msf[i]
        y = y_sv[i]
        x = y_cc[i]
        ax.text(i-w, z+0.1, str(z), horizontalalignment='center')
        ax.text(i  , y+0.1, str(y), horizontalalignment='center')
        ax.text(i+w, x+0.1, str(x), horizontalalignment='center')
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Number of iterations", fontsize=14)
    ax.set_ylim(ymin=0, ymax=18)
    ax.legend(loc="upper right", fontsize=12)
    fig.savefig("iterations.pdf", bbox_inches="tight")

def plot_CC_runtime():
    w = 0.25
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    y_cc = [dic["CC"][g]["fast-cc"][1] for g in graphs]
    y_cm = [dic["CC"][g]["comb-sv64"][1] for g in graphs]
    y_cm2 = [dic["CC"][g]["comb-sv16"][1] for g in graphs]
    sp = [y_cm[i] / y_cc[i] for i in range(0, 5)]
    xs = [i-w*1.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm2, width=w, label="CombBLAS-FastSV (16x4)", color=colors[1]);
    xs = [i-w*0.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm, width=w, label="CombBLAS-FastSV (64x1)", color=colors[0]);
    xs = [i+w*0.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="Boruvka-CC (8x8)", color=colors[2]);
    sp = ["%.2fx" % (y_cm[i] / y_cc[i]) for i in range(0, len(graphs))]
    print "CC ", sp, (sum(y_cm) / sum(y_cc))
    for i in range(0, len(graphs)):
        z = y_cm2[i]
        y = y_cm[i]
        x = y_cc[i]
        ax.text(i-w, z+0.15, ("%.1f" % z), horizontalalignment='center', fontsize=9)
        ax.text(i-0, y+0.15, ("%.1f" % y), horizontalalignment='center', fontsize=9)
        ax.text(i+w, x+0.15, ("%.1f" % x), horizontalalignment='center', fontsize=9)
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Time (s)", fontsize=14)
    ax.legend(loc="upper left", fontsize=11)
    fig.savefig("CC-time.pdf", bbox_inches="tight")

def plot_CC_norm_time():
    w = 0.25
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    y_cc = [dic["CC"][g]["fast-cc"][1] / dic["CC"][g]["fast-cc"][0] for g in graphs]
    y_cm = [dic["CC"][g]["comb-sv64"][1] / dic["CC"][g]["comb-sv64"][0] for g in graphs]
    y_cm2 = [dic["CC"][g]["comb-sv16"][1] / dic["CC"][g]["comb-sv16"][0] for g in graphs]
    sp = [y_cm[i] / y_cc[i] for i in range(0, 5)]
    xs = [i-w*1.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm2, width=w, label="CombBLAS-FastSV (16x4)", color=colors[1]);
    xs = [i-w*0.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm, width=w, label="CombBLAS-FastSV (64x1)", color=colors[0]);
    xs = [i+w*0.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="Boruvka-CC (8x8)", color=colors[2]);
    sp = ["%.2fx" % (y_cm[i] / y_cc[i]) for i in range(0, len(graphs))]
    print "CCn", sp, (sum(y_cm) / sum(y_cc))
    return
    for i in range(0, len(graphs)):
        z = y_cm2[i]
        y = y_cm[i]
        x = y_cc[i]
        ax.text(i-w, z+0.05, ("%.1f" % z), horizontalalignment='center')
        ax.text(i-0, y+0.05, ("%.1f" % y), horizontalalignment='center')
        ax.text(i+w, x+0.05, ("%.1f" % x), horizontalalignment='center')
        #ax.text(i-w/2, y+0.05, ("%.1f" % y), horizontalalignment='center')
        #ax.text(i+w/2, x+0.05, ("%.1f" % x), horizontalalignment='center')
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Time (s)", fontsize=14)
    ax.legend(loc="upper left", fontsize=12)
    fig.savefig("CC-norm-time.pdf", bbox_inches="tight")

def plot_MSF_runtime():
    w = 0.40
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    y_fast = [dic["MSF"][g]["fast-msf"][1] for g in graphs]
    y_msf1 = [dic["MSF"][g]["msf1"][1] for g in graphs]
    sp = [y_msf1[i] / y_fast[i] for i in range(0, 5)]
    xs = [i-w for i in range(0, len(graphs))]
    ax.bar(xs, y_msf1, width=w, label="Pregel-MSF (64x1)", color=colors[0]);
    xs = [i for i in range(0, len(graphs))]
    ax.bar(xs, y_fast, width=w, label="Boruvka-MSF (8x8)", color=colors[2]);
    sp = ["%.2fx" % (y_msf1[i] / y_fast[i]) for i in range(0, len(graphs))]
    print "MSF", sp, (sum(y_msf1) / sum(y_fast))
    for i in range(0, len(graphs)):
        y = y_msf1[i]
        x = y_fast[i]
        ax.text(i-w/2, y+1, ("%.1f" % y), horizontalalignment='center')
        ax.text(i+w/2, x+1, ("%.1f" % x), horizontalalignment='center')
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Time (s)", fontsize=14)
    ax.legend(loc="upper left", fontsize=12)
    fig.savefig("MSF-time.pdf", bbox_inches="tight")

def plot_both_runtime():
    fig, axes = plt.subplots(1, 2, figsize=(11, 3))
    w = 0.27
    ax = axes[1]
    y_cc = [dic["CC"][g]["fast-cc"][1] for g in graphs]
    y_cm = [dic["CC"][g]["comb-sv64"][1] for g in graphs]
    y_cm2 = [dic["CC"][g]["comb-sv16"][1] for g in graphs]
    sp = [y_cm[i] / y_cc[i] for i in range(0, 5)]
    xs = [i-w*1.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm2, width=w, label="CombBLAS-FastSV (16x4)", color=colors[1]);
    xs = [i-w*0.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm, width=w, label="CombBLAS-FastSV (64x1)", color=colors[0]);
    xs = [i+w*0.5 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="Boruvka-CC (8x8)", color=colors[2]);
    sp = ["%.2fx" % (y_cm[i] / y_cc[i]) for i in range(0, len(graphs))]
    print sp, (sum(y_cm) / sum(y_cc))
    for i in range(0, len(graphs)):
        z = y_cm2[i]
        y = y_cm[i]
        x = y_cc[i]
        ax.text(i-w, z+0.15, ("%.1f" % z), horizontalalignment='center', fontsize=8)
        ax.text(i-0, y+0.15, ("%.1f" % y), horizontalalignment='center', fontsize=8)
        ax.text(i+w, x+0.15, ("%.1f" % x), horizontalalignment='center', fontsize=8)
    xs = [i for i in range(0, len(graphs))]
    ax.set_title("Connected Component", fontsize=15)
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Time (s)", fontsize=14)
    ax.legend(loc="upper left", fontsize=10)

    w = 0.35
    ax = axes[0]
    y_fast = [dic["MSF"][g]["fast-msf"][1] for g in graphs]
    y_msf1 = [dic["MSF"][g]["msf1"][1] for g in graphs]
    sp = [y_msf1[i] / y_fast[i] for i in range(0, 5)]
    xs = [i-w for i in range(0, len(graphs))]
    ax.bar(xs, y_msf1, width=w, label="Pregel-MSF", color=colors[0]);
    xs = [i for i in range(0, len(graphs))]
    ax.bar(xs, y_fast, width=w, label="Boruvka-MSF", color=colors[2]);
    sp = ["%.2fx" % (y_msf1[i] / y_fast[i]) for i in range(0, len(graphs))]
    print sp, (sum(y_msf1) / sum(y_fast))
    for i in range(0, len(graphs)):
        y = y_msf1[i]
        x = y_fast[i]
        ax.text(i-w/2, y+1, ("%.1f" % y), horizontalalignment='center', fontsize=8)
        ax.text(i+w/2, x+1, ("%.1f" % x), horizontalalignment='center', fontsize=8)
    xs = [i for i in range(0, len(graphs))]
    ax.set_title("Minimum Spanning Forest", fontsize=15)
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Time (s)", fontsize=14)
    ax.legend(loc="upper left", fontsize=10)
    fig.savefig("runtime.pdf", bbox_inches="tight")

def plot_CC_speedup():
    nts = [1, 2, 4, 8]
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    for g in graphs:
        ys = [dic_mt["CC"][g][1] / dic_mt["CC"][g][nt] for nt in nts]
        ax.plot(nts, ys, marker="v", label=g)
    ax.set_xlabel("number of threads")
    ax.set_xticks(nts)
    ax.set_xticklabels([str(nt) for nt in nts], fontsize=14)
    #ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Speedup", fontsize=14)
    ax.legend(loc="upper left", fontsize=12)
    fig.savefig("CC-speedup.pdf", bbox_inches="tight")

def plot_MSF_speedup():
    nts = [1, 2, 4, 8]
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    for g in graphs:
        ys = [dic_mt["MSF"][g][1] / dic_mt["MSF"][g][nt] for nt in nts]
        ax.plot(nts, ys, marker="v", label=g)
    ax.set_xlabel("number of threads")
    ax.set_xticks(nts)
    ax.set_xticklabels([str(nt) for nt in nts], fontsize=14)
    #ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Speedup", fontsize=14)
    ax.legend(loc="upper left", fontsize=12)
    fig.savefig("MSF-speedup.pdf", bbox_inches="tight")

def figure_grid(rows, cols):
    fig, axarr = plt.subplots(rows, cols, figsize=(cols*5, rows*4))
    if (rows > 1 and cols > 1):
        axlist = []
        for i in range(0, rows):
            for j in range(0, cols):
                axlist.append(axarr[i, j])
    else:
        axlist = axarr
    return fig, axlist

def plot_active(prob, fname):
    ns = []
    fig, axlist = figure_grid(1, 5)
    for i in range(0, 5):
        graph = graphs[i]
        ax = axlist[i]
        nvals = dic_ac[prob][graph][0]
        verts = dic_ac[prob][graph][1]
        #print graph, nums[i], verts[0], ("%.2f%%" % (verts[0] * 100.0 / nums[i]))
        print graph, sum(verts), ("%.2f\\%%" % (sum(verts) * 100.0 / nums[i]))
        iters = len(verts)
        log_e = [math.log(y, 2) for y in nvals]
        log_v = [math.log(y, 2) for y in verts]
        x_ticks = range(0, iters)
        x_labels = [str(i + 1) for i in x_ticks]
        xs = [i-0.4 for i in range(0, iters)]
        ax.set_title(graph, fontsize=22)
        ax.bar(xs, log_e, 0.8)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        #ax.set_yticks(y_ticks)
        #ax.set_yticklabels(y_labels)
        #ax.set_ylim(ymin = 0, ymax = 110)
        ax.set_xlabel("Iteration", fontsize=22)
        ax.tick_params(labelsize=16)

        xs = [i for i in range(0, iters)]
        ax2 = ax.twinx()
        ax2.plot(xs, log_v, marker="o", markersize=9, linewidth=3, color="red")
        ax2.set_xlim(xmin=-0.5, xmax=iters-0.5)
        ax2.tick_params(labelsize=16)
    fig.text(-0.015, 0.5, "# elements (log)", fontsize=22, va="center", rotation="vertical")
    fig.text(1.0, 0.5, "# supervertices (log)", fontsize=22, va="center", rotation="vertical")
    #fig.text(0.5, -0.01, "Number of cores", fontsize=18, ha="center")
    fig.tight_layout()
    fig.savefig(fname, bbox_inches="tight")
    fig.clf()

def plot_active_both():
    plot_active("CC", "iter-active-CC.pdf")
    plot_active("MSF", "iter-active-MSF.pdf")


def plot_CC_runtime_2():
    w = 0.19
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    y_cc = [dic["CC"][g]["fast-cc"][1] for g in graphs]
    y_cc2 = [dic["CC"][g]["fast-cc8"][1] for g in graphs]
    y_cm = [dic["CC"][g]["comb-sv64"][1] for g in graphs]
    y_cm2 = [dic["CC"][g]["comb-sv16"][1] for g in graphs]
    sp = [y_cm[i] / y_cc[i] for i in range(0, 5)]
    xs = [i-w*2 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm2, width=w, label="CombBLAS-FastSV (16x4)", color=colors[1]);
    xs = [i-w*1 for i in range(0, len(graphs))]
    ax.bar(xs, y_cm, width=w, label="CombBLAS-FastSV (64x1)", color=colors[0]);
    xs = [i+w*0 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc2, width=w, label="Boruvka-CC (8x4)", color=colors[3]);
    xs = [i+w*1 for i in range(0, len(graphs))]
    ax.bar(xs, y_cc, width=w, label="Boruvka-CC (8x8)", color=colors[2]);
    sp = ["%.2fx" % (y_cm[i] / y_cc[i]) for i in range(0, len(graphs))]
    print sp, (sum(y_cm) / sum(y_cc))
    for i in range(0, len(graphs)):
        z = y_cm2[i]
        y = y_cm[i]
        t = y_cc2[i]
        x = y_cc[i]
        ax.text(i-w*1.5, z+0.15, ("%.1f" % z), horizontalalignment='center', fontsize=9)
        ax.text(i-w*0.5, y+0.15, ("%.1f" % y), horizontalalignment='center', fontsize=9)
        ax.text(i+w*0.5, t+0.15, ("%.1f" % t), horizontalalignment='center', fontsize=9)
        ax.text(i+w*1.5, x+0.15, ("%.1f" % x), horizontalalignment='center', fontsize=9)
    xs = [i for i in range(0, len(graphs))]
    ax.set_xticks(xs)
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylabel("Time (s)", fontsize=14)
    ax.legend(loc="upper left", fontsize=11)
    fig.savefig("CC-time.pdf", bbox_inches="tight")

def plot_CC_breakdown():
    w = 0.7
    fig, axlist = figure_grid(1, 5)
    for idx in range(0, len(graphs)):
        graph = graphs[idx]
        ys = [[], [], [], []]  # y[op][cores]
        hs = [[], [], [], []]
        tt = [0, 0, 0, 0]
        cores = [1, 2, 4, 8]
        cc_dic = dic_pa_cc["CC"][graph]
        for i in range(0, len(cores)):
            for j in range(0, 4):  # mxv, assign, others, extract
                ys[j].append(cc_dic[cores[i]][j])
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])
            tt[j] = hs[0][j] + ys[0][j]

        xs = [i-w/2 for i in range(0, len(cores))]
        ax = axlist[idx]
        ax.set_title(graph, fontsize=22)
        labels = ["mxv", "assign", "extract", "others"]
        for i in range(0, 4):
            ax.bar(xs, ys[i], w, hs[i], label=labels[i], color=colors[i], zorder=3)
        line = "%8s" % graph
        for j in range(0, 4):
            line += " | %s %5.2fx" % (labels[j], ys[j][0] / ys[j][3])
        print line
        for i in range(1, 4):
            sp = "%.2fx" % (tt[0] / tt[i])
            ax.text(i, tt[i]+tt[0]*0.02, sp, horizontalalignment='center', fontsize=16)
        x_ticks = [i for i in range(0, len(cores))]
        x_labels = [str(i) for i in cores]
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.tick_params(labelsize=16)
        ax.legend(fontsize=18)
        #ax.yaxis.grid(True, zorder=0)

    fig.text(-0.01, 0.5, "Time (s)", fontsize=22, va="center", rotation="vertical")
    fig.text(0.5, -0.01, "Number of threads allocated on each process", fontsize=24, ha="center")
    fig.tight_layout()
    fig.savefig("CC-runtime-breakdown.pdf", bbox_inches="tight")
    fig.clf()

def plot_MSF_breakdown():
    w = 0.7
    fig, axlist = figure_grid(1, 5)
    for idx in range(0, len(graphs)):
        graph = graphs[idx]
        ys = [[], [], [], []]  # y[op][cores]
        hs = [[], [], [], []]
        tt = [0.0, 0.0, 0.0, 0.0]
        cores = [1, 2, 4, 8]
        cc_dic = dic_pa_cc["MSF"][graph]
        for i in range(0, len(cores)):
            for j in range(0, 4):  # mxv, assign, others, extract
                ys[j].append(cc_dic[cores[i]][j])
        for j in range(0, len(ys[0])):
            hs[3].append(0)
            for i in [2, 1, 0]:
                hs[i].append(hs[i + 1][j] + ys[i + 1][j])
            tt[j] = hs[0][j] + ys[0][j]

        xs = [i-w/2 for i in range(0, len(cores))]
        ax = axlist[idx]
        ax.set_title(graph, fontsize=22)
        labels = ["mxv", "assign", "extract", "others"]
        for i in range(0, 4):
            ax.bar(xs, ys[i], w, hs[i], label=labels[i], color=colors[i], zorder=3)
        line = "%8s" % graph
        for j in range(0, 4):
            line += " | %s %5.2fx" % (labels[j], ys[j][0] / ys[j][3])
        print line
        for i in range(1, 4):
            sp = "%.2fx" % (tt[0] / tt[i])
            ax.text(i, tt[i]+0.02*tt[0], sp, horizontalalignment='center', fontsize=16)
        x_ticks = [i for i in range(0, len(cores))]
        x_labels = [str(i) for i in cores]
        ax.set_xlim(xmin=x_ticks[0]-0.5, xmax=x_ticks[-1]+0.5)
        ax.set_xticks(x_ticks)
        ax.set_xticklabels(x_labels)
        ax.tick_params(labelsize=16)
        ax.legend(fontsize=18)
        #ax.yaxis.grid(True, zorder=0)

    fig.text(-0.01, 0.5, "Time (s)", fontsize=22, va="center", rotation="vertical")
    fig.text(0.5, -0.01, "Number of threads allocated on each process", fontsize=24, ha="center")
    fig.tight_layout()
    fig.savefig("MSF-runtime-breakdown.pdf", bbox_inches="tight")
    fig.clf()

def plot_breakdown_cmp():
    labels = ["mxv", "assign", "extract", "others"]
    w = 0.38
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    xs = []
    ys = [[], [], [], []]
    hs = [[], [], [], []]
    for idx in range(0, len(graphs)):
        graph = graphs[idx]
        v1 = dic_pa_cc["CC"][graph][8]
        v2 = dic_pa_sv["CC"][graph][8]
        x1 = float(idx) - w
        x2 = float(idx)
        xs.append(x1)
        xs.append(x2)
        l1 = [(v1[i] / dic["CC"][graph]["fast-cc"][0]) for i in range(0, 4)]
        l2 = [(v2[i] / dic["CC"][graph]["comb-sv64"][0]) for i in range(0, 4)]
        h1 = sum(l1)
        h2 = sum(l2)
        t1 = dic["CC"][graph]["fast-cc"][1] / dic["CC"][graph]["fast-cc"][0]
        t2 = dic["CC"][graph]["comb-sv64"][1] / dic["CC"][graph]["comb-sv64"][0]
        n1 = t1 / t2 / sum(l1)
        n2 = t2 / t2 / sum(l2)
        ax.text(x1+w*0.45, t1/t2+0.02, ("%.2f" % t1), horizontalalignment='center', fontsize=12)
        ax.text(x2+w*0.45, t2/t2+0.02, ("%.2f" % t2), horizontalalignment='center', fontsize=12)
        for i in range(0, 4):
            x = v1[i] / dic["CC"][graph]["fast-cc"][0]
            y = v2[i] / dic["CC"][graph]["comb-sv64"][0]
            h1 -= x
            h2 -= y
            ys[i].append(x * n1)
            ys[i].append(y * n2)
            hs[i].append(h1 * n1)
            hs[i].append(h2 * n2)
    for i in range(0, 4):
        ax.bar(xs, ys[i], w*0.9, hs[i], label=labels[i], color=colors[i])

    ax.legend(fontsize=12, ncol=4)
    ax.set_xlim(xmin=-0.5, xmax=4.5)
    ax.set_ylim(ymin=0.0, ymax=1.4)
    ax.set_xticks([0, 1, 2, 3, 4])
    ax.set_xticklabels(graphs, fontsize=14)
    ax.set_ylabel("Time (s)")
    ax.set_title("Boruvka-CC (Left) v.s. FastSV (Right)", fontsize=14)
    for labs in ax.get_yticklabels():
        labs.set_visible(False)
    fig.tight_layout()
    fig.savefig("CC-breakdown.pdf", bbox_inches="tight")
    fig.clf()

def show_breakdown():
    ops = ("mxv", "assign", "extract", "others")
    lines = ["mxv", "assign", "extract", "others"]
    sx = [0.0, 0.0, 0.0, 0.0]
    sy = [0.0, 0.0, 0.0, 0.0]
    for graph in graphs:
        v1 = dic_pa_cc["CC"][graph][8]
        v2 = dic_pa_sv["CC"][graph][8]
        #print "%10s %10s %10s" % ("operation", "BoruvkaCC", "FastSV")
        for i in range(0, 4):
            op = ops[i]
            x = v1[i] / dic["CC"][graph]["fast-cc"][0]
            y = v2[i] / dic["CC"][graph]["comb-sv64"][0]
            lines[i] += " & %.4f & %.4f" % (x, y)
            sx[i] += x
            sy[i] += y
            #print "%10s %10.4f %10.4f" % (op, v1[i] / dic["CC"][graph]["fast-cc"][0], v2[i] / dic["CC"][graph]["comb-sv64"][0])
    for i in range(0, 4):
        print lines[i] + "\\\\"
        print "\\hline"
    for i in range(0, 4):
        print ops[i], sy[i], sx[i], sy[i] / sx[i]


parse("CC-Boruvka-0228.txt", "CC", "fast-cc")
parse3("CC-combblas16-0131.txt", "CC", "comb-sv16")
parse3("CC-combblas64-0130.txt", "CC", "comb-sv64")

parse("MSF-FastMSF-0228.txt", "MSF", "fast-msf")
parse2("MSF-Pregel-0215.txt", "MSF")

parse4("CC-Boruvka-MT-0229.txt", "CC")
parse4("MSF-FastMSF-MT-0228.txt", "MSF")

# active supervertex, active edges
#parse5("CC-Boruvka-0228.txt", "CC")
#parse5("MSF-FastMSF-0228.txt", "MSF")

#plot_iters()
#plot_CC_runtime()
#plot_MSF_runtime()
#plot_both_runtime()
#plot_active_both()

#plot_CC_speedup()
#plot_CC_norm_time()
#plot_MSF_speedup()
#plot_CC_runtime_2()

plot_CC_breakdown()
plot_MSF_breakdown()
#show_breakdown()
#plot_breakdown_cmp()
