# road fast-msf 8
filename: /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
finish reading: 0.517178s
iteration 1: nvals 57708624 mxv 0.101755 assign 0.000000 update 0.052446 path 0.075824 active 7117557
iteration 2: nvals 23490996 mxv 0.133100 assign 0.484825 update 0.068242 path 0.090025 active 2047008
iteration 3: nvals 11027838 mxv 0.098351 assign 0.290324 update 0.049046 path 0.070443 active 542906
iteration 4: nvals 5096842 mxv 0.081850 assign 0.186919 update 0.031585 path 0.052363 active 132105
iteration 5: nvals 2207642 mxv 0.060861 assign 0.154808 update 0.021781 path 0.043221 active 30197
iteration 6: nvals 926664 mxv 0.052882 assign 0.114480 update 0.013241 path 0.025321 active 6717
iteration 7: nvals 389610 mxv 0.049665 assign 0.069522 update 0.008519 path 0.017719 active 1479
iteration 8: nvals 168212 mxv 0.048725 assign 0.061526 update 0.006516 path 0.016639 active 315
iteration 9: nvals 70336 mxv 0.047870 assign 0.031958 update 0.005703 path 0.015718 active 68
iteration 10: nvals 29398 mxv 0.047624 assign 0.038894 update 0.005479 path 0.012025 active 14
iteration 11: nvals 13862 mxv 0.047468 assign 0.017150 update 0.005243 path 0.012914 active 3
iteration 12: nvals 1676 mxv 0.047336 assign 0.016652 update 0.005150 path 0.015680 active 1
number of components: 1
elapsed time: 3.262189
total weight: 59505686128
Sendrecv 0.084015
# web fast-msf 8
filename: /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
finish reading: 14.499048s
iteration 1: nvals 3620126660 mxv 0.890424 assign 0.000000 update 0.093722 path 0.162204 active 3820666
iteration 2: nvals 3497039338 mxv 1.500103 assign 0.517050 update 0.073605 path 0.120716 active 254392
iteration 3: nvals 1335723534 mxv 1.375999 assign 0.180208 update 0.036835 path 0.042809 active 18359
iteration 4: nvals 183251872 mxv 0.589867 assign 0.084177 update 0.017321 path 0.039961 active 1473
iteration 5: nvals 6536290 mxv 0.199389 assign 0.048934 update 0.011851 path 0.018364 active 92
iteration 6: nvals 231962 mxv 0.107261 assign 0.038645 update 0.012758 path 0.009090 active 6
iteration 7: nvals 1224 mxv 0.099776 assign 0.034049 update 0.010853 path 0.005629 active 1
number of components: 123
elapsed time: 6.808387
total weight: 1385336161
Sendrecv 0.224425
# twitter fast-msf 8
filename: /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
finish reading: 11.765163s
iteration 1: nvals 2405026092 mxv 0.712691 assign 0.000000 update 0.094455 path 0.164454 active 2493051
iteration 2: nvals 2324441138 mxv 1.145132 assign 0.549920 update 0.100856 path 0.148269 active 53992
iteration 3: nvals 1080910058 mxv 1.164568 assign 0.098069 update 0.050763 path 0.049051 active 155
iteration 4: nvals 608490 mxv 0.420244 assign 0.056714 update 0.043823 path 0.006996 active 1
number of components: 19926186
elapsed time: 5.283464
total weight: 1224063176
Sendrecv 0.222544
# kron fast-msf 8
filename: /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
finish reading: 18.475618s
iteration 1: nvals 4223264644 mxv 1.223435 assign 0.000000 update 0.174401 path 0.373491 active 1805144
iteration 2: nvals 4098003590 mxv 2.131371 assign 0.832558 update 0.177157 path 0.233221 active 3879
iteration 3: nvals 2655410438 mxv 2.153322 assign 0.144871 update 0.115417 path 0.093927 active 1
number of components: 71164263
elapsed time: 8.544526
total weight: 3808683550
Sendrecv 0.508342
# urand fast-msf 8
filename: /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
finish reading: 21.184363s
iteration 1: nvals 4294966740 mxv 2.334322 assign 0.000000 update 0.401923 path 0.498731 active 33510596
iteration 2: nvals 4093552384 mxv 3.695264 assign 2.726285 update 0.435415 path 0.845956 active 6828028
iteration 3: nvals 4040186456 mxv 3.579719 assign 1.500946 update 0.294790 path 0.572506 active 1153902
iteration 4: nvals 4028832534 mxv 3.523189 assign 1.142579 update 0.185425 path 0.395567 active 164034
iteration 5: nvals 4026806218 mxv 3.518953 assign 0.662866 update 0.085870 path 0.217609 active 18296
iteration 6: nvals 4025995572 mxv 3.513727 assign 0.296332 update 0.042028 path 0.084048 active 1517
iteration 7: nvals 4004901502 mxv 3.531397 assign 0.161779 update 0.030338 path 0.085558 active 140
iteration 8: nvals 2990544528 mxv 3.687717 assign 0.095906 update 0.029058 path 0.088276 active 11
iteration 9: nvals 1818715158 mxv 3.185005 assign 0.088225 update 0.029112 path 0.034424 active 1
number of components: 1
elapsed time: 43.299588
total weight: 1353519497
Sendrecv 0.530015
