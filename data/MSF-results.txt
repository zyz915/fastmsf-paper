# road msf1
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
reading /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
Load time 1.485723 seconds
|V| = 23947347, |E| = 57708624
Iteration 1: active count 3566275
Iteration 2: active count 1025934
Iteration 3: active count 271998
Iteration 4: active count 66415
Iteration 5: active count 15206
Iteration 6: active count 3355
Iteration 7: active count 737
Iteration 8: active count 171
Iteration 9: active count 34
Iteration 10: active count 8
Iteration 11: active count 2
Iteration 12: active count 0
Total Number of Supersteps: 159
Total Running Time: 3.602842 second
Total Message Size: 4291454100 Bytes (4.29 GB)
number of components: 1
total weight of MSF: 59505686128
road fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
finish reading: 1.091298s
iteration 1: nvals 57708624 mxv 0.213383 assign 0.000000 update 0.052398 path 0.051827
iteration 2: nvals 23490996 mxv 0.356979 assign 0.069988 update 0.070654 path 0.071563
iteration 3: nvals 11027838 mxv 0.262423 assign 0.072772 update 0.047448 path 0.061915
iteration 4: nvals 5096842 mxv 0.223462 assign 0.068680 update 0.029622 path 0.050097
iteration 5: nvals 2207642 mxv 0.201091 assign 0.053468 update 0.019957 path 0.037101
iteration 6: nvals 926664 mxv 0.191730 assign 0.038523 update 0.013914 path 0.024291
iteration 7: nvals 389610 mxv 0.190965 assign 0.031551 update 0.010654 path 0.017093
iteration 8: nvals 168212 mxv 0.194844 assign 0.026433 update 0.009132 path 0.015952
iteration 9: nvals 70336 mxv 0.189152 assign 0.025207 update 0.008507 path 0.014945
iteration 10: nvals 29398 mxv 0.188570 assign 0.024516 update 0.008226 path 0.011206
iteration 11: nvals 13862 mxv 0.186381 assign 0.024478 update 0.007985 path 0.012326
iteration 12: nvals 1676 mxv 0.186536 assign 0.024323 update 0.007967 path 0.015085
iteration 13: nvals 0 mxv 0.184120
number of components: 1
elapsed time: 8.000333
total weight: 59505686128
Sendrecv 3.885889, Allgather 0.000000, Ibcast 0.000000
# web msf1
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
reading /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
Load time 29.451001 seconds
|V| = 50636151, |E| = 3620126660
Iteration 1: active count 2971757
Iteration 2: active count 197895
Iteration 3: active count 14461
Iteration 4: active count 1256
Iteration 5: active count 84
Iteration 6: active count 5
Iteration 7: active count 0
Total Number of Supersteps: 85
Total Running Time: 36.533652 second
Total Message Size: 28040197388 Bytes (28.04 GB)
number of components: 123
total weight of MSF: 1385336161
web fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
finish reading: 11.583511s
iteration 1: nvals 3620126660 mxv 0.998546 assign 0.000000 update 0.090645 path 0.149121
iteration 2: nvals 3497039338 mxv 1.662387 assign 0.224081 update 0.079708 path 0.101615
iteration 3: nvals 1335723534 mxv 1.758801 assign 0.140642 update 0.036140 path 0.039906
iteration 4: nvals 183251872 mxv 0.984154 assign 0.082120 update 0.022235 path 0.038157
iteration 5: nvals 6536290 mxv 0.539348 assign 0.054900 update 0.017535 path 0.017558
iteration 6: nvals 231962 mxv 0.399932 assign 0.052543 update 0.016692 path 0.008351
iteration 7: nvals 1224 mxv 0.391833 assign 0.051579 update 0.016691 path 0.004871
iteration 8: nvals 0 mxv 0.388021
number of components: 123
elapsed time: 13.236732
total weight: 1385336161
Sendrecv 4.562269, Allgather 0.000000, Ibcast 0.000000
# twitter msf1
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
reading /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
Load time 20.984878 seconds
|V| = 61578414, |E| = 2405026092
Iteration 1: active count 2046723
Iteration 2: active count 53058
Iteration 3: active count 154
Iteration 4: active count 0
Total Number of Supersteps: 49
Total Running Time: 26.072815 second
Total Message Size: 16996146672 Bytes (17.00 GB)
number of components: 19926185
total weight of MSF: 1224063176
twitter fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
finish reading: 9.245722s
iteration 1: nvals 2405026092 mxv 1.035637 assign 0.000000 update 0.091725 path 0.157360
iteration 2: nvals 2324441138 mxv 1.627420 assign 0.228089 update 0.080043 path 0.126044
iteration 3: nvals 1080910058 mxv 1.765419 assign 0.105958 update 0.028638 path 0.044498
iteration 4: nvals 608490 mxv 0.872619 assign 0.069523 update 0.020199 path 0.005789
iteration 5: nvals 0 mxv 0.473831
number of components: 19926186
elapsed time: 10.021144
total weight: 1224063176
Sendrecv 3.001449, Allgather 0.000000, Ibcast 0.000000
# kron msf1
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
reading /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
Load time 32.435829 seconds
|V| = 134217724, |E| = 4223264644
Iteration 1: active count 1676777
Iteration 2: active count 3877
Iteration 3: active count 0
Total Number of Supersteps: 39
Total Running Time: 42.733311 second
Total Message Size: 19361291388 Bytes (19.36 GB)
number of components: 71164261
total weight of MSF: 3808683550
kron fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
finish reading: 19.790936s
iteration 1: nvals 4223264644 mxv 2.079157 assign 0.000000 update 0.166635 path 0.352966
iteration 2: nvals 4098003590 mxv 2.993571 assign 0.447684 update 0.136848 path 0.205584
iteration 3: nvals 2655410438 mxv 3.111275 assign 0.207133 update 0.046015 path 0.091380
iteration 4: nvals 0 mxv 1.969532
number of components: 71164263
elapsed time: 16.818077
total weight: 3808683550
Sendrecv 4.589423, Allgather 0.000000, Ibcast 0.000000
# urand msf1
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
reading /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
Load time 40.516922 seconds
|V| = 134217728, |E| = 4294966740
Iteration 1: active count 17605321
Iteration 2: active count 3770462
Iteration 3: active count 695167
Iteration 4: active count 113744
Iteration 5: active count 15235
Iteration 6: active count 1345
Iteration 7: active count 126
Iteration 8: active count 9
Iteration 9: active count 0
Total Number of Supersteps: 117
Total Running Time: 163.462391 second
Total Message Size: 243675355972 Bytes (243.68 GB)
number of components: 1
total weight of MSF: 1353519497
urand fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
finish reading: 20.760336s
iteration 1: nvals 4294966740 mxv 3.519799 assign 0.000000 update 0.390657 path 0.460426
iteration 2: nvals 4093552384 mxv 5.370183 assign 0.523492 update 0.445137 path 0.603463
iteration 3: nvals 4040186456 mxv 5.519272 assign 0.731904 update 0.268594 path 0.516583
iteration 4: nvals 4028832534 mxv 5.301386 assign 0.794068 update 0.147163 path 0.381095
iteration 5: nvals 4026806218 mxv 5.146637 assign 0.747026 update 0.080990 path 0.211764
iteration 6: nvals 4025995572 mxv 5.172520 assign 0.418036 update 0.051085 path 0.081056
iteration 7: nvals 4004901502 mxv 5.267793 assign 0.197894 update 0.044684 path 0.081976
iteration 8: nvals 2990544528 mxv 5.272841 assign 0.141077 update 0.043711 path 0.085685
iteration 9: nvals 1818715158 mxv 4.727237 assign 0.140822 update 0.043550 path 0.032183
iteration 10: nvals 0 mxv 2.418227
number of components: 1
elapsed time: 72.446336
total weight: 1353519497
Sendrecv 15.718547, Allgather 0.000000, Ibcast 0.000000
