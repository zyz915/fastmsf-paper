# road fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
finish reading: 0.568069s
iteration 1: nvals 57708624 mxv 0.100588 assign 0.000000 update 0.051945 path 0.076611 active 7117557
iteration 2: nvals 23490996 mxv 0.142672 assign 0.469751 update 0.067421 path 0.094673 active 2047008
iteration 3: nvals 11027838 mxv 0.099156 assign 0.286229 update 0.048676 path 0.071299 active 542906
iteration 4: nvals 5096842 mxv 0.076598 assign 0.197130 update 0.030951 path 0.053722 active 132105
iteration 5: nvals 2207642 mxv 0.060936 assign 0.168496 update 0.021520 path 0.038591 active 30197
iteration 6: nvals 926664 mxv 0.052893 assign 0.117779 update 0.013391 path 0.024860 active 6717
iteration 7: nvals 389610 mxv 0.049607 assign 0.071295 update 0.008295 path 0.017648 active 1479
iteration 8: nvals 168212 mxv 0.048066 assign 0.059047 update 0.006490 path 0.017205 active 315
iteration 9: nvals 70336 mxv 0.047599 assign 0.028000 update 0.005764 path 0.015897 active 68
iteration 10: nvals 29398 mxv 0.047598 assign 0.032574 update 0.005488 path 0.011910 active 14
iteration 11: nvals 13862 mxv 0.046979 assign 0.015526 update 0.005265 path 0.013239 active 3
iteration 12: nvals 1676 mxv 0.046837 assign 0.014781 update 0.005180 path 0.015579 active 1
iteration 13: nvals 0 mxv 0.046757
number of components: 1
elapsed time: 3.315644
total weight: 59505686128
Sendrecv 0.092287
# web fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
finish reading: 14.741746s
iteration 1: nvals 3620126660 mxv 0.870389 assign 0.000000 update 0.092013 path 0.156437 active 3820758
iteration 2: nvals 3497039338 mxv 1.486207 assign 0.716897 update 0.072104 path 0.130464 active 254392
iteration 3: nvals 1335723534 mxv 1.357849 assign 0.394403 update 0.037213 path 0.047205 active 18359
iteration 4: nvals 183251872 mxv 0.569703 assign 0.075474 update 0.017241 path 0.039709 active 1473
iteration 5: nvals 6536290 mxv 0.200499 assign 0.050332 update 0.011859 path 0.018562 active 92
iteration 6: nvals 231962 mxv 0.107300 assign 0.038611 update 0.010850 path 0.009571 active 6
iteration 7: nvals 1224 mxv 0.100641 assign 0.033867 update 0.010668 path 0.006679 active 1
iteration 8: nvals 0 mxv 0.100569
number of components: 123
elapsed time: 7.254031
total weight: 1385336161
Sendrecv 0.223924
# twitter fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
finish reading: 11.941511s
iteration 1: nvals 2405026092 mxv 0.684531 assign 0.000000 update 0.092530 path 0.159676 active 22419236
iteration 2: nvals 2324441138 mxv 1.135744 assign 1.311847 update 0.102000 path 0.148046 active 53992
iteration 3: nvals 1080910058 mxv 1.160076 assign 0.099275 update 0.051493 path 0.047561 active 155
iteration 4: nvals 608490 mxv 0.409352 assign 0.057616 update 0.044306 path 0.007583 active 1
iteration 5: nvals 0 mxv 0.121309
number of components: 19926186
elapsed time: 6.138727
total weight: 1224063176
Sendrecv 0.238985
# kron fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
finish reading: 19.208625s
iteration 1: nvals 4223264644 mxv 1.205844 assign 0.000000 update 0.169231 path 0.367468 active 72948886
iteration 2: nvals 4098003590 mxv 2.157345 assign 3.777985 update 0.177117 path 0.246367 active 3879
iteration 3: nvals 2655410438 mxv 2.175486 assign 0.136306 update 0.114507 path 0.094897 active 1
iteration 4: nvals 0 mxv 0.893344
number of components: 71164263
elapsed time: 12.444845
total weight: 3808683550
Sendrecv 0.518395
# urand fast-msf
filename: /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
finish reading: 21.340511s
iteration 1: nvals 4294966740 mxv 2.350317 assign 0.000000 update 0.396517 path 0.485806 active 33510596
iteration 2: nvals 4093552384 mxv 3.755976 assign 2.810899 update 0.431787 path 0.799194 active 6828028
iteration 3: nvals 4040186456 mxv 3.666504 assign 1.599047 update 0.290323 path 0.617609 active 1153902
iteration 4: nvals 4028832534 mxv 3.671608 assign 1.086224 update 0.172444 path 0.421666 active 164034
iteration 5: nvals 4026806218 mxv 3.596489 assign 0.689683 update 0.084859 path 0.221623 active 18296
iteration 6: nvals 4025995572 mxv 3.602540 assign 0.272480 update 0.042338 path 0.084756 active 1517
iteration 7: nvals 4004901502 mxv 3.599878 assign 0.142107 update 0.030163 path 0.084794 active 140
iteration 8: nvals 2990544528 mxv 3.770221 assign 0.096397 update 0.028599 path 0.088064 active 11
iteration 9: nvals 1818715158 mxv 3.270804 assign 0.086700 update 0.028512 path 0.034961 active 1
iteration 10: nvals 0 mxv 1.474513
number of components: 1
elapsed time: 45.572264
total weight: 1353519497
Sendrecv 0.510836
