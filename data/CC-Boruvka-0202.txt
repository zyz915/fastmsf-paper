# road fast-cc
filename: /home/ubuntu/graphs/par64r/GAP-road/GAP-road.wel
finish reading: 1.081671s
iteration 1: nvals 57708624 mxv 0.171448 assign 0.000000 update 0.045045 path 0.085531 active_S 4911559
iteration 2: nvals 17582122 mxv 0.236699 assign 0.154963 update 0.041278 path 0.067464 active_S 730811
iteration 3: nvals 5151620 mxv 0.208149 assign 0.068833 update 0.015564 path 0.025518 active_S 78224
iteration 4: nvals 1551214 mxv 0.195847 assign 0.017931 update 0.006872 path 0.016589 active_S 6780
iteration 5: nvals 484082 mxv 0.192405 assign 0.010823 update 0.004844 path 0.014676 active_S 552
iteration 6: nvals 152662 mxv 0.185220 assign 0.009935 update 0.004811 path 0.012367 active_S 45
iteration 7: nvals 43760 mxv 0.184092 assign 0.009383 update 0.004765 path 0.011232 active_S 6
iteration 8: nvals 12464 mxv 0.183919 assign 0.009193 update 0.004761 path 0.015776 active_S 2
iteration 9: nvals 2678 mxv 0.182048 assign 0.009111 update 0.004805 path 0.011816 active_S 1
iteration 10: nvals 0 mxv 0.182481
number of components: 1
elapsed time: 3.418058
Sendrecv 0.801489
# web fast-cc
filename: /home/ubuntu/graphs/par64r/GAP-web/GAP-web.wel
finish reading: 11.858095s
iteration 1: nvals 3620126660 mxv 0.749671 assign 0.000000 update 0.064631 path 0.116872 active_S 694424
iteration 2: nvals 1080494338 mxv 1.296161 assign 0.100329 update 0.022763 path 0.040476 active_S 4363
iteration 3: nvals 232387182 mxv 0.748741 assign 0.041355 update 0.010426 path 0.048419 active_S 18
iteration 4: nvals 17576 mxv 0.457567 assign 0.020230 update 0.010025 path 0.009959 active_S 1
iteration 5: nvals 0 mxv 0.384624
number of components: 123
elapsed time: 4.967757
Sendrecv 0.825957
# twitter fast-cc
filename: /home/ubuntu/graphs/par64r/GAP-twitter/GAP-twitter.wel
finish reading: 9.658394s
iteration 1: nvals 2405026092 mxv 0.858597 assign 0.000000 update 0.095037 path 0.138346 active_S 434993
iteration 2: nvals 2176665390 mxv 1.204817 assign 0.092757 update 0.047960 path 0.053188 active_S 4
iteration 3: nvals 16 mxv 0.826296 assign 0.032052 update 0.040173 path 0.007390 active_S 1
iteration 4: nvals 0 mxv 0.464643
number of components: 19926186
elapsed time: 4.602961
Sendrecv 0.717832
# kron fast-cc
filename: /home/ubuntu/graphs/par64r/GAP-kron/GAP-kron.wel
finish reading: 19.971692s
iteration 1: nvals 4223264644 mxv 1.729620 assign 0.000000 update 0.211347 path 0.319659 active_S 372053
iteration 2: nvals 4008994986 mxv 2.434886 assign 0.157815 update 0.105964 path 0.122740 active_S 1
iteration 3: nvals 0 mxv 1.715370
number of components: 71164263
elapsed time: 7.909340
Sendrecv 1.056999
# urand fast-cc
filename: /home/ubuntu/graphs/par64r/GAP-urand/GAP-urand.wel
finish reading: 20.727549s
iteration 1: nvals 4294966740 mxv 2.394587 assign 0.000000 update 0.137530 path 0.332996 active_S 2098440
iteration 2: nvals 4030724634 mxv 3.714512 assign 0.214993 update 0.061670 path 0.109189 active_S 143
iteration 3: nvals 3901039460 mxv 3.501887 assign 0.049586 update 0.026208 path 0.038899 active_S 1
iteration 4: nvals 0 mxv 2.292119
number of components: 1
elapsed time: 14.408503
Sendrecv 1.479371