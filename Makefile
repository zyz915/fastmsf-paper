all:paper

minted:
	pdflatex -shell-escape paper.tex
	bibtex paper.aux
	pdflatex -shell-escape paper.tex
	pdflatex -shell-escape paper.tex

full:
	pdflatex paper.tex
	bibtex paper.aux
	pdflatex paper.tex
	pdflatex -shell-escape paper.tex

paper:
	pdflatex paper.tex
