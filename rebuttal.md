We appreciate the reviewers' comments and suggestions. Below we respond to the main concerns raised in the reviews.

### About the memory consumption and its impact (on scalability)

Several reviewers concern about memory consumption and its impact. From section 4.1 we can imply that the total memory consumption is O(np+m) where O(m) is for the edges and O(np) is for the duplicated vectors (n, m, p: number of vertices, edges, and processes). Suppose M is the memory space of a single node, then for memory requirement, we should have "Mp > np+m" or "M > n+m/p". We can see that at "M > n" is necessary in our solution (but m can eventually fit into the memory by increasing p) and we consider it viable since for most practical graphs n is typically small (see Section 4.1 and Table 1). We agree that including a detailed analysis can make our paper more friendly to readers.

As for the impact, the O(np) appeared in the memory consumption (and also computation and communication cost) indeed limits the scalability of our solution. Using one process per node is always preferable for our solution, and this is the reason we do so in our experiments. At another extreme when p>O(m/n), increasing p will not bring a speedup. However, for practice graphs, m is usually one or two orders of magnitude larger, and using OpenMP as the second level parallelism can further improve the scalability.



### How does the presented algorithm fare with a single node MST or CC code? (#2)

For MST, our solution has the optimal time complexity O(mlog(n)) with a single node (see our related work), since the best parallel MST algorithms is also based on Boruvka's algorithm. However, this paper focuses on the distributed-memory system and we did not conduct an empirical comparison with those shared-memory solutions.

For CC, there are both theoretically or empirically linear time parallel solution with a single node. In our related work, we said that Boruvka's algorithm does not provide the optimal solution for CC on shared-memory.

### About the communication complexity (reviewer #3)

Our linear algebra implementation of Boruvka's algorithm reduces the communication cost from O(mlog(n)) to O(np). We noticed that reviewer #3 misunderstood this point and considered the Pregel solution to have O(m) communication cost. We should clarify that in Pregel O(m) is the communication cost per iteration (see the last sentence in 3rd paragraph of the introduction or section 5.4.1) and in section 3.4 we know that Boruvka's algorithm terminates in O(log n) rounds.

Then, our solution reduces the communication complexity in most practical scenarios. It is constantly better than Pregel on small clusters. Even we use O(m/n) processes to avoid performance degradation, Pregel's communication cost is still O(log n) times larger than our solution.

### About how our algorithm is implemented (reviewer #3)

We first map Boruvka's algorithm to linear algebra operations and then implement each operation manually using MPI and OpenMP (see the beginning sentence in Section 4). To be clear, our work does not rely on any existing library or API but manually implemented a small set of simplified APIs defined in section 3.1. We will make our code accessible in our final version.

### Pregel requires a lot of communication because the indices are randomly permuted. Does a random permutation of the indices not completely destroy any locality? (reviewer #4)

Pregel and its variations randomly assign each vertex to a machine/process using a hash function (see section 5.1), in which case the expected communication cost is O(m*(p-1)/p) or simply O(m) regardless of how the graph looks like. A random permutation of the indices will not likely change the communication cost significantly.

### What is the savings of the mxv operation in CC, and do the performance differences reflect those savings? (reviewer #5)

The simplified CC algorithm (algorithm 2) has less memory overhead since it stores less information on each vertex. In particular, the datatype of vector es and ev in CC are integers (line 2, algorithm 2) but in MSF they are tuples (line 12, algorithm 1). The main impact is the reduction in communication costs for the assign operation, but the mxv should run faster as well.

In section 5.2 and 5.4, we show that CC converges faster than MSF and has less active supervertices during the execution. Therefore, the data type is not the only thing that causes the performance difference between CC and MSF.